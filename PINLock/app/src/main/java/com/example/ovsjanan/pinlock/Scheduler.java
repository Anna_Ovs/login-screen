package com.example.ovsjanan.pinlock;

import android.content.Context;

/**
 * Determines the order in which the tasks are performed.
 *
 * Created by ovsjanan on 17.08.2017.
 */

public class Scheduler {

    private String userName;
    private int userId;
    private int trialNr;
    private int latinSquare [][];
    private static Scheduler instance;

    private int latinSquare8 [][] = {
            {1,2,3,4,5,6,7,8},
            {2,3,4,5,6,7,8,1},
            {3,4,5,6,7,8,1,2},
            {4,5,6,7,8,1,2,3},
            {5,6,7,8,1,2,3,4},
            {6,7,8,1,2,3,4,5},
            {7,8,1,2,3,4,5,6},
            {8,1,2,3,4,5,6,7},
    };

    public Scheduler(Context context) {
        instance = this;
    }

    // Singleton
    public static Scheduler getInstance(Context context) {

        if (instance == null)
            instance = new Scheduler(context);
        return instance;
    }

    public Scheduler(int userId, String userName) {

        this.userId = userId;
        this.userName = userName;
        this.trialNr = 0;
        this.latinSquare = latinSquare8;
    }

    public int getNextTrial(){

        return this.latinSquare[(this.userId-1) % latinSquare.length][trialNr];
    }

    public int getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public void progressTrial(){

        this.trialNr++;
        if(this.trialNr >= latinSquare8.length-1){
            this.trialNr = latinSquare8.length-1;
        }
    }
}
