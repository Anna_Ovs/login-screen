package com.example.ovsjanan.pinlock;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Displays the settings screen when starting the app.
 *
 * Created by ovsjanan on 17.08.2017.
 */

public class MainActivity extends AppCompatActivity {

    public static Scheduler scheduler;
    public static int trial = 10;
    public static final String TRIAL_MODE = "com.example.ovsjanan.pinlock.TRIAL_MODE";
    public static final String USER_NAME = "com.example.ovsjanan.pinlock.USER_NAME";
    public static final String USER_ID = "com.example.ovsjanan.pinlock.USER_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {};

    public void clickStartButton (View view) {

        EditText user_name = (EditText) findViewById(R.id.edit_name);
        String userName = user_name.getText().toString();

        EditText user_id = (EditText) findViewById(R.id.edit_id);
        int userID = Integer.parseInt(user_id.getText().toString());

        //TextView trial = (TextView) findViewById(R.id.auto_trial);
        //String auto_trial = trial.getText().toString();

        scheduler = new Scheduler(userID, userName);

        // Get UI
        UISwitcher.getInstance(MainActivity.this).getNextUI(MainActivity.this);
    }

    public void clickDownloadButton (View view) {
        DBHelper dbHelper = DBHelper.getInstance(this);
        boolean result = dbHelper.exportDB();
        if (result) {
            Toast.makeText(this, "DB exported to external storage.",
                    Toast.LENGTH_LONG).show();
        }
    }
}