package com.example.ovsjanan.pinlock;

import android.provider.BaseColumns;

/**
 * Created by ovsjanan on 22.08.2017.
 */

public class DBStructure {

    public DBStructure() {}

    public static abstract class Columns implements BaseColumns {

        // Events table
        public static final String TABLE_EVENTS = "events";
        public static final String EVENTS_COL_ID = "id";
        public static final String EVENTS_COL_PIN = "pin";
        public static final String EVENTS_COL_ITERATION = "iteration";
        public static final String EVENTS_COL_HIT = "hit";
        public static final String EVENTS_COL_TOUCH_DOWN_X = "touchDownX";
        public static final String EVENTS_COL_TOUCH_DOWN_Y = "touchDownY";
        public static final String EVENTS_COL_TOUCH_UP_X = "touchUpX";
        public static final String EVENTS_COL_TOUCH_UP_Y = "touchUpY";
        public static final String EVENTS_COL_TIME_DOWN = "timeDown";
        public static final String EVENTS_COL_TIME_UP = "timeUp";
        public static final String EVENTS_COL_PRESSURE_DOWN = "pressureDown";
        public static final String EVENTS_COL_PRESSURE_UP = "pressureUp";
        public static final String EVENTS_COL_SIZE_DOWN = "sizeDown";
        public static final String EVENTS_COL_SIZE_UP = "sizeUp";
        public static final String EVENTS_COL_ACC_X = "accelerometerX";
        public static final String EVENTS_COL_ACC_Y = "accelerometerY";
        public static final String EVENTS_COL_ACC_Z = "accelerometerZ";
        public static final String EVENTS_COL_GYR_X = "gyroscopeX";
        public static final String EVENTS_COL_GYR_Y = "gyroscopeY";
        public static final String EVENTS_COL_GYR_Z = "gyroscopeZ";
        public static final String EVENTS_COL_AZIMUTH = "azimuth";
        public static final String EVENTS_COL_PITCH = "pitch";
        public static final String EVENTS_COL_ROLL = "roll";
        public static final String EVENTS_COL_MINOR_UP = "minorUp";
        public static final String EVENTS_COL_MINOR_DOWN = "minorDown";
        public static final String EVENTS_COL_MAJOR_UP = "majorUp";
        public static final String EVENTS_COL_MAJOR_DOWN = "majorDown";
        public static final String EVENTS_COL_LAYOUT_WIDTH = "layoutWidth";
        public static final String EVENTS_COL_LAYOUT_HEIGHT = "layoutHeight";
        public static final String EVENTS_COL_TRIAL = "trial";
        public static final String EVENTS_COL_INITIAL_TIME = "initialTime";

        // Trials table
        public static final String TABLE_TRIALS = "trials";
        public static final String TRIALS_COL_TRIAL_COMBI = "trial";
        public static final String TRIALS_COL_NAME = "userName";
        public static final String TRIALS_COL_UI = "ui";
        public static final String TRIALS_COL_POSTURE = "posture";
        public static final String TRIALS_COL_SUBJECT_ID = "userId";
    }
}