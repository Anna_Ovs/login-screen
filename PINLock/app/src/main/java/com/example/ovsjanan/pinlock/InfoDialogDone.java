package com.example.ovsjanan.pinlock;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import static com.example.ovsjanan.pinlock.AbstractUI.returnToMainActivity;

/**
 * Displays the final done dialog.
 *
 * Created by ovsjanan on 05.09.2017.
 * */

@SuppressLint("ValidFragment")
public class InfoDialogDone extends DialogFragment {

    private final AbstractUI aUI;

    public InfoDialogDone(AbstractUI aUI) {this.aUI = aUI;}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setTitle("INFO")
                .setMessage(getString(R.string.trial_done))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        aUI.resetTrialFinished();
                        returnToMainActivity();
                    }
                }).create();
    }
}