package com.example.ovsjanan.pinlock;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Displays the next UI depending on current trial.
 *
 * Created by ovsjanan on 04.09.2017.
 */

public class UISwitcher {

    private int posture;
    private int ui;
    private int trial;
    public int caseCounter = 0;
    private String userName;
    private int userID;
    private static UISwitcher instance;
    public static int trialDB;
    private Context context;
    Activity a = (Activity) context;
    public boolean trialFinished = false;

    public UISwitcher (Activity a) {
        this.a = a;
        instance = this;
    }

    // Singleton
    public static UISwitcher getInstance(Activity a) {
        if (instance == null)
            instance = new UISwitcher(a);
        return instance;
    }

    public int getPosture() {
        return this.posture;
    }

    public String getTrial() {return String.valueOf(this.trial);}

    public int getIntTrial() {return this.trial;}

    public void resetTrialFinished() {
        this.trialFinished = false;
        this.caseCounter = 0;
    }

    public void getNextUI(Context context) {

        Log.v("UISwitcher", "getNextUI()___trialFinished: " + trialFinished);

            this.userName = MainActivity.scheduler.getUserName();
            this.trial = MainActivity.scheduler.getNextTrial();
            this.userID = MainActivity.scheduler.getUserId();

            switch (this.trial) {
                case 1:
                    posture = 0;
                    ui = 0;
                    caseCounter++;
                    Intent intent1 = new Intent(context, DefaultUI.class);
                    a.startActivity(intent1);
                    break;
                case 2:
                    posture = 0;
                    ui = 1;
                    caseCounter++;
                    Intent intent2 = new Intent(context, MiniUI.class);
                    a.startActivity(intent2);
                    break;
                case 3:
                    posture = 0;
                    ui = 2;
                    caseCounter++;
                    Intent intent3 = new Intent(context, MiniStretchedUI.class);
                    a.startActivity(intent3);
                    break;
                case 4:
                    posture = 0;
                    ui = 3;
                    caseCounter++;
                    Intent intent4 = new Intent(context, DefaultStretchedUI.class);
                    a.startActivity(intent4);
                    break;
                case 5:
                    posture = 1;
                    ui = 0;
                    caseCounter++;
                    Intent intent5 = new Intent(context, DefaultUI.class);
                    a.startActivity(intent5);
                    break;
                case 6:
                    posture = 1;
                    ui = 1;
                    caseCounter++;
                    Intent intent6 = new Intent(context, DefaultStretchedUI.class);
                    a.startActivity(intent6);
                    break;
                case 7:
                    posture = 1;
                    ui = 2;
                    caseCounter++;
                    Intent intent7 = new Intent(context, MiniUI.class);
                    a.startActivity(intent7);
                    break;
                case 8:
                    posture = 1;
                    ui = 3;
                    caseCounter++;
                    Intent intent8 = new Intent(context, MiniStretchedUI.class);
                    a.startActivity(intent8);
                    break;
        }

        MainActivity.scheduler.progressTrial();
        Log.v("UISwitcher", "CASE_COUNTER: " + caseCounter);
        // trial finished
        if (caseCounter == 9) {

            trialFinished = true;

        } else {

            this.trialDB = DBHelper.getInstance(a).insertTrial(this.trial, this.userID, this.userName, this.ui,
                    this.posture);

            Log.v("UISwitcher", "Insert Trial = " + "trial: " + trial + " Username: "
                    + this.userName + " User-ID: " + this.userID + " UI: " + ui + " Posture:" + posture
                    + " User ID: " + this.userID);
        }
    }
}