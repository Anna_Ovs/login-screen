package com.example.ovsjanan.pinlock;

import android.os.Bundle;

/**
 * Loads the layout of the DefaultStretchedUI.
 *
 * Created by ovsjanan on 24.08.2017.
 */

public class DefaultStretchedUI extends AbstractUI {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!UISwitcher.getInstance(DefaultStretchedUI.this).trialFinished) {
            displayDialog();

        } else {

            InfoDialogDone id = new InfoDialogDone(this);
            id.show(getFragmentManager(), "Done Dialog");
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.default_stretched_layout;
    }
}
