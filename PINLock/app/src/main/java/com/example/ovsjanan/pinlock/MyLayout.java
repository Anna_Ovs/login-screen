package com.example.ovsjanan.pinlock;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;


/**
 * Custom view, which intercepts the onTouch events from its children (i.e. buttons).
 *
 * Created by ovsjanan on 28.08.2017.
 */

public class MyLayout extends RelativeLayout {

    private float downX;
    private float downY;
    private float upX;
    private float upY;
    private float pressureDown;
    private float pressureUp;
    private float sizeDown;
    private float sizeUp;
    private float majorDown;
    private float majorUp;
    private float minorDown;
    private float minorUp;
    private float layoutWidth;
    private float layoutHeight;
    private float accX;
    private float accY;
    private float accZ;
    private float gyrX;
    private float gyrY;
    private float gyrZ;
    private float azimuth;
    private float pitch;
    private float roll;
    private long timeDown;
    private long timeUp;
    private int trial;
    private static MyLayout instance;
    MyLayout myLayout = (MyLayout) findViewById(R.id.content_layout);

    Button button0;
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;

    public MyLayout(Context context) {
        super(context);
        instance = this;
    }

    public MyLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        instance = this;
    }

    public MyLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
        instance = this;
    }

    private void init(Context context) {}

    // Singleton
    public static MyLayout getInstance(Context context) {
        if (instance == null)
            instance = new MyLayout(context);
        return instance;
    }

    // Tell the layout to consume only the touch events (not the onClick events) from its children.
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {

        boolean result = super.onInterceptTouchEvent(event);
        if (onTouchEvent(event)) {
            return result;
        } else {
            return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN && getParent() != null) {

            getParent().requestDisallowInterceptTouchEvent(true);

            downX = event.getX();
            downY = event.getY();
            timeDown = System.currentTimeMillis();
            pressureDown = event.getPressure();
            sizeDown = event.getSize();
            majorDown = event.getTouchMajor();
            minorDown = event.getTouchMinor();

        } else if (event.getAction() == MotionEvent.ACTION_UP) {

            upX = event.getX();
            upY = event.getY();
            timeUp = System.currentTimeMillis();
            pressureUp = event.getPressure();
            sizeUp = event.getSize();
            majorUp = event.getTouchMajor();
            minorUp = event.getTouchMinor();
            layoutWidth = myLayout.getWidth();
            layoutHeight = myLayout.getHeight();

            accX = AbstractUI.a_values.get(AbstractUI.a_values.size() - 3);
            accY = AbstractUI.a_values.get(AbstractUI.a_values.size() - 2);
            accZ = AbstractUI.a_values.get(AbstractUI.a_values.size() - 1);
            gyrX = AbstractUI.g_values.get(AbstractUI.g_values.size() - 3);
            gyrY = AbstractUI.g_values.get(AbstractUI.g_values.size() - 2);
            gyrZ = AbstractUI.g_values.get(AbstractUI.g_values.size() - 1);
            azimuth = AbstractUI.o_values.get(AbstractUI.o_values.size() - 3);
            pitch = AbstractUI.o_values.get(AbstractUI.o_values.size() - 2);
            roll = AbstractUI.o_values.get(AbstractUI.o_values.size() - 1);
            trial = ((AbstractUI)this.getContext()).getTrial();

            // Center of Buttons (for later python script)
            button0 = (Button) findViewById(R.id.button0);
            button1 = (Button) findViewById(R.id.button1);
            button2 = (Button) findViewById(R.id.button2);
            button3 = (Button) findViewById(R.id.button3);
            button4 = (Button) findViewById(R.id.button4);
            button5 = (Button) findViewById(R.id.button5);
            button6 = (Button) findViewById(R.id.button6);
            button7 = (Button) findViewById(R.id.button7);
            button8 = (Button) findViewById(R.id.button8);
            button9 = (Button) findViewById(R.id.button9);

            Point centerPoint0 = getCenterOfButton(button0);
            Point centerPoint1 = getCenterOfButton(button1);
            Point centerPoint2 = getCenterOfButton(button2);
            Point centerPoint3 = getCenterOfButton(button3);
            Point centerPoint4 = getCenterOfButton(button4);
            Point centerPoint5 = getCenterOfButton(button5);
            Point centerPoint6 = getCenterOfButton(button6);
            Point centerPoint7 = getCenterOfButton(button7);
            Point centerPoint8 = getCenterOfButton(button8);
            Point centerPoint9 = getCenterOfButton(button9);

            Log.v("MyLayout", "BUTTON LOCATION 0:" + " x:" + centerPoint0.x + " y:" + centerPoint0.y + "\n" +
                              "BUTTON LOCATION 1:" + " x:" + centerPoint1.x + " y:" + centerPoint1.y + "\n" +
                              "BUTTON LOCATION 2:" + " x:" + centerPoint2.x + " y:" + centerPoint2.y + "\n" +
                              "BUTTON LOCATION 3:" + " x:" + centerPoint3.x + " y:" + centerPoint3.y + "\n" +
                              "BUTTON LOCATION 4:" + " x:" + centerPoint4.x + " y:" + centerPoint4.y + "\n" +
                              "BUTTON LOCATION 5:" + " x:" + centerPoint5.x + " y:" + centerPoint5.y + "\n" +
                              "BUTTON LOCATION 6:" + " x:" + centerPoint6.x + " y:" + centerPoint6.y + "\n" +
                              "BUTTON LOCATION 7:" + " x:" + centerPoint7.x + " y:" + centerPoint7.y + "\n" +
                              "BUTTON LOCATION 8:" + " x:" + centerPoint8.x + " y:" + centerPoint8.y + "\n" +
                              "BUTTON LOCATION 9:" + " x:" + centerPoint9.x + " y:" + centerPoint9.y);
        }

        return super.onTouchEvent(event);
    }

    private Point getCenterOfButton(View view) {

        int[] location = new int[2];
        view.getLocationInWindow(location);
        int x = location[0] + view.getWidth() / 2;
        int y = location[1] - view.getHeight() / 2;

        return new Point(x, y);
    }

    public void onHit(String pin, float hit, long initialTime, float iteration) {

        Log.v("MyLayout", "TOUCH OCCURED = " +
                " PIN: " + pin +
                " Iteration: " + iteration +
                " Hit: " + hit +
                " Initial Time: " + initialTime +
                " Pressure Down: " + pressureDown + " Pressure Up: " + pressureUp +
                " X: " + downX + " Y: " + downY +
                " Size Down: " + sizeDown + " Size Up: " + sizeUp +
                " Ellipse Major Down: " + majorDown + " Ellipse Major Up: " + majorUp +
                " Ellipse Minor Down: " + minorDown + " Ellipse Minor Up: " + minorUp +
                " Time Down: " + timeDown + " Time Up: " + timeUp +
                " AccX: " + accX + " AccY: " + accY + " AccZ: " + accZ +
                " GyrX: " + gyrX + " GyrY: " + gyrY + " GyrZ: " + gyrZ +
                " Azimuth: " + azimuth + " Pitch: " + pitch + " Roll: " + roll +
                " Layout Width: " + layoutWidth + " Layout Height: " + layoutHeight +
                " Trial ID: " + trial);

        this.commitEvent(pin, iteration, hit, downX, downY, upX, upY, initialTime, timeDown, timeUp, pressureDown, pressureUp,
                sizeDown, sizeUp, accX, accY, accZ, gyrX, gyrY, gyrZ, azimuth, pitch, roll,
                minorDown, minorUp, majorDown, majorUp, layoutWidth, layoutHeight, trial);
    }

    public void commitEvent(String pin, float iteration, float hit, float downX, float downY, float upX, float upY,
                            long initialTime, long timeDown, long timeUp, float pressureDown,
                            float pressureUp, float sizeDown, float sizeUp, float accX, float accY,
                            float accZ, float gyrX, float gyrY, float gyrZ, float azimuth, float pitch,
                            float roll, float minorDown, float minorUp, float majorDown, float majorUp,
                            float layoutWidth, float layoutHeight, int trial) {

        DBHelper.getInstance(this.getContext()).insertEvents(pin, iteration, hit, downX, downY, upX,
                upY, initialTime, timeDown, timeUp, pressureDown, pressureUp,
                sizeDown, sizeUp, accX, accY, accZ, gyrX, gyrY,
                gyrZ, azimuth, pitch, roll, minorDown, minorUp,
                majorDown, majorUp, layoutWidth, layoutHeight, trial);
    }
}