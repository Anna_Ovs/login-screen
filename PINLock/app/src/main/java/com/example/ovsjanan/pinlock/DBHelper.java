package com.example.ovsjanan.pinlock;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

/**
 * Utility class for creating and upating the SQLite database.
 *
 * Created by ovsjanan on 07.08.2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG = DBHelper.class.getSimpleName();
    private static final String SQL_DELETE_TRIALS_ENTRIES = "DROP TABLE IF EXISTS " + DBStructure.Columns.TABLE_TRIALS;
    private static final String SQL_DELETE_EVENTS_ENTRIES = "DROP TABLE IF EXISTS " + DBStructure.Columns.TABLE_EVENTS;
    public static final String DB_NAME = "PinLockDB.db";
    public static final int DB_VERSION = 5;
    public static final String SQL_CREATE_EVENTS =
            "CREATE TABLE " + DBStructure.Columns.TABLE_EVENTS + " ("
                    + DBStructure.Columns.EVENTS_COL_ID + " INTEGER PRIMARY KEY,"
                    + DBStructure.Columns.EVENTS_COL_PIN + " TEXT,"
                    + DBStructure.Columns.EVENTS_COL_ITERATION + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_HIT + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_TOUCH_DOWN_X + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_TOUCH_DOWN_Y + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_TOUCH_UP_X + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_TOUCH_UP_Y + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_INITIAL_TIME + " TIMESTAMP NOT NULL DEFAULT current_timestamp,"
                    + DBStructure.Columns.EVENTS_COL_TIME_DOWN + " INTEGER,"
                    + DBStructure.Columns.EVENTS_COL_TIME_UP + " INTEGER,"
                    + DBStructure.Columns.EVENTS_COL_PRESSURE_DOWN + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_PRESSURE_UP + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_SIZE_DOWN + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_SIZE_UP + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_ACC_X + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_ACC_Y + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_ACC_Z + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_GYR_X + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_GYR_Y + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_GYR_Z + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_AZIMUTH + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_PITCH + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_ROLL + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_MINOR_DOWN + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_MINOR_UP + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_MAJOR_DOWN + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_MAJOR_UP + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_LAYOUT_WIDTH + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_LAYOUT_HEIGHT + " REAL,"
                    + DBStructure.Columns.EVENTS_COL_TRIAL + " INTERGER)";

    private static final String SQL_CREATE_TRIALS =
            "CREATE TABLE " + DBStructure.Columns.TABLE_TRIALS + " ("
                    + DBStructure.Columns.EVENTS_COL_ID + " INTEGER PRIMARY KEY,"
                    + DBStructure.Columns.TRIALS_COL_TRIAL_COMBI + " INTEGER, "
                    + DBStructure.Columns.TRIALS_COL_NAME + " TEXT, "
                    + DBStructure.Columns.TRIALS_COL_UI + " INTEGER, "
                    + DBStructure.Columns.TRIALS_COL_POSTURE + " INTEGER, "
                    + DBStructure.Columns.TRIALS_COL_SUBJECT_ID + " INTEGER)";

    // Constructor
    public DBHelper (Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Log.d(LOG_TAG, "DbHelper hat die Datenbank " + getDatabaseName() +
                " mit der Versionsnummer " + DB_VERSION + " erzeugt.");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.d(LOG_TAG, "Tabelle wird mit SQL-Befehl: " + SQL_CREATE_EVENTS + " angelegt.");
            Log.d(LOG_TAG, "Tabelle wird mit SQL-Befehl: " + SQL_CREATE_TRIALS + " angelegt.");
            db.execSQL(SQL_CREATE_EVENTS);
            db.execSQL(SQL_CREATE_TRIALS);
        }
        catch (Exception ex) {
            Log.e(LOG_TAG, "Fehler beim Anlegen der Tabelle: " + ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(SQL_DELETE_EVENTS_ENTRIES);
        db.execSQL(SQL_DELETE_TRIALS_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    // Insert values into TRIAL table in DB.
    public int insertTrial(int trial,  int userID, String userName, int ui, int posture) {

        Log.d("DBHelper", SQL_CREATE_TRIALS);

        SQLiteDatabase db = this.getWritableDatabase();

        // Column names are the keys
        ContentValues values = new ContentValues();
        values.put(DBStructure.Columns.TRIALS_COL_TRIAL_COMBI, trial);
        values.put(DBStructure.Columns.TRIALS_COL_NAME, userName);
        values.put(DBStructure.Columns.TRIALS_COL_UI, ui);
        values.put(DBStructure.Columns.TRIALS_COL_POSTURE, posture);
        values.put(DBStructure.Columns.TRIALS_COL_SUBJECT_ID, userID);

        // Insert row, returning the primary key
        int newRowId;
        newRowId = (int) db.insert(
                DBStructure.Columns.TABLE_TRIALS,
                null,
                values);

        return newRowId;
    }

    // Insert values into EVENTS table in DB.
    public float insertEvents(String pin, float iteration, float hit, float downX, float downY, float upX, float upY,
                              long initialTime, long timeDown, long timeUp, float pressureDown,
                              float pressureUp, float sizeDown, float sizeUp, float accX, float accY,
                              float accZ, float gyrX, float gyrY, float gyrZ, float azimuth,
                              float pitch, float roll, float minorDown, float minorUp, float majorDown,
                              float majorUp, float layoutWidth, float layoutHeight, int trial) {

        Log.d("DBHelper", SQL_CREATE_EVENTS);
        SQLiteDatabase db = this.getWritableDatabase();
        //  pin speichern
        // Column names are the keys
        ContentValues values = new ContentValues();
        values.put(DBStructure.Columns.EVENTS_COL_PIN, pin);
        values.put(DBStructure.Columns.EVENTS_COL_ITERATION, iteration);
        values.put(DBStructure.Columns.EVENTS_COL_HIT, hit);
        values.put(DBStructure.Columns.EVENTS_COL_TOUCH_DOWN_X, downX);
        values.put(DBStructure.Columns.EVENTS_COL_TOUCH_DOWN_Y, downY);
        values.put(DBStructure.Columns.EVENTS_COL_TOUCH_UP_X, upX);
        values.put(DBStructure.Columns.EVENTS_COL_TOUCH_UP_Y, upY);
        values.put(DBStructure.Columns.EVENTS_COL_INITIAL_TIME, initialTime);
        values.put(DBStructure.Columns.EVENTS_COL_TIME_DOWN, timeDown);
        values.put(DBStructure.Columns.EVENTS_COL_TIME_UP, timeUp);
        values.put(DBStructure.Columns.EVENTS_COL_PRESSURE_DOWN, pressureDown);
        values.put(DBStructure.Columns.EVENTS_COL_PRESSURE_UP, pressureUp);
        values.put(DBStructure.Columns.EVENTS_COL_SIZE_DOWN, sizeDown);
        values.put(DBStructure.Columns.EVENTS_COL_SIZE_UP, sizeUp);
        values.put(DBStructure.Columns.EVENTS_COL_ACC_X, accX);
        values.put(DBStructure.Columns.EVENTS_COL_ACC_Y, accY);
        values.put(DBStructure.Columns.EVENTS_COL_ACC_Z, accZ);
        values.put(DBStructure.Columns.EVENTS_COL_GYR_X, gyrX);
        values.put(DBStructure.Columns.EVENTS_COL_GYR_Y, gyrY);
        values.put(DBStructure.Columns.EVENTS_COL_GYR_Z, gyrZ);
        values.put(DBStructure.Columns.EVENTS_COL_AZIMUTH, azimuth);
        values.put(DBStructure.Columns.EVENTS_COL_PITCH, pitch);
        values.put(DBStructure.Columns.EVENTS_COL_ROLL, roll);
        values.put(DBStructure.Columns.EVENTS_COL_MINOR_DOWN, minorDown);
        values.put(DBStructure.Columns.EVENTS_COL_MINOR_UP, minorUp);
        values.put(DBStructure.Columns.EVENTS_COL_MAJOR_DOWN, majorDown);
        values.put(DBStructure.Columns.EVENTS_COL_MAJOR_UP, majorUp);
        values.put(DBStructure.Columns.EVENTS_COL_LAYOUT_WIDTH, layoutWidth);
        values.put(DBStructure.Columns.EVENTS_COL_LAYOUT_HEIGHT, layoutHeight);
        values.put(DBStructure.Columns.EVENTS_COL_TRIAL, trial);

        // Insert row, returning the primary key
        long newRowId;
        newRowId = db.insert(
                DBStructure.Columns.TABLE_EVENTS,
                null,
                values);

        return newRowId;
    }

    // Singleton
    private static DBHelper instance;
    public static DBHelper getInstance(Context context) {

        if (instance == null)
            instance = new DBHelper(context);

        return instance;
    }

    public boolean exportDB() {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + "com.example.ovsjanan.pinlock"
                        + "//databases//" + DB_NAME;
                String backupDBPath  = DB_NAME;
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

                Log.d("RESTORE", backupDB.toString());
                return true;
            }
        } catch (Exception e) {
            Log.d("RESTORE", e.toString());
        }

        return false;
    }
}