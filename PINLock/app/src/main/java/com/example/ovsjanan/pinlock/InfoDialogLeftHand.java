package com.example.ovsjanan.pinlock;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Displays the dialog for the left hand posture.
 *
 * Created by ovsjanan on 04.09.2017.
 */

@SuppressLint("ValidFragment")
public class InfoDialogLeftHand extends DialogFragment {

    private final AbstractUI aUI;

    public InfoDialogLeftHand (AbstractUI aUI) {
        this.aUI = aUI;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setTitle("INFO")
                .setMessage(getResources().getText(R.string.posture0))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        aUI.setInitialTime();
                        dismiss();
                    }
                }).create();
    }
}
