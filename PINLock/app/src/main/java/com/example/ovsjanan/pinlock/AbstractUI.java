package com.example.ovsjanan.pinlock;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Abstract class implements the basic functions for the UIs.
 *
 * Created by ovsjanan on 24.08.2017.
 */

public abstract class AbstractUI extends AppCompatActivity {

    String userEntered;
    String currentPin;
    List <String> pins = Arrays.asList("1537", "1479", "3681", "2058");
    final int PIN_LENGTH = 4;
    private int pinCounter = 0;
    private int pinIndex = 0;
    private int attemptCounter = 0;
    private int trial;
    public float hit;
    public long initialTime;
    public float iteration = 1;

    Context appContext;
    TextView passwordField;
    TextView statusView;
    TextView currentTrial;
    EditText passwordInput;
    Button button0;
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;

    private float aX;
    private float aY;
    private float aZ;
    private float gX;
    private float gY;
    private float gZ;
    private float azimuth;
    private float pitch;
    private float roll;

    static ArrayList<Float> a_values = new ArrayList<Float>();
    static ArrayList<Float> g_values = new ArrayList<Float>();
    static ArrayList<Float> o_values = new ArrayList<Float>();

    private Sensor sensor;
    private SensorManager SM;
    private static Activity activity = null;
    private MyLayout myLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        appContext = this;
        userEntered = "";
        currentPin = pins.get(0);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

        // Load layout of sub-activity
        setContentView(this.getLayoutResource());
        setMyLayout();

        passwordField = (TextView) findViewById(R.id.password_field);
        passwordField.setText("Enter Password: " + currentPin);
        statusView = (TextView) findViewById(R.id.statusview);
        passwordInput = (EditText) findViewById(R.id.editText);
        currentTrial = (TextView) findViewById(R.id.trial);
        currentTrial.setText(UISwitcher.getInstance(AbstractUI.this).getTrial());

        // Register sensors
        SM = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        SM.registerListener(sensorListener, SM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        SM.registerListener(sensorListener, SM.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_NORMAL);
        SM.registerListener(sensorListener, SM.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_NORMAL);

        // Check pin
        MyLayout.OnClickListener pinButtonHandler = new com.example.ovsjanan.pinlock.MyLayout.OnClickListener() {

            public void onClick(View v) {

                Button pressedButton = (Button) v;
                if (userEntered.length() < PIN_LENGTH) {

                    userEntered = userEntered + pressedButton.getText();
                    Log.v("AbstractUI", "User entered= " + userEntered);
                    Log.v("AbstractUI", "CURRENT PIN= " + currentPin);

                    // Update pin boxes
                    passwordInput.setText(passwordInput.getText().toString() + "*");
                    passwordInput.setSelection(passwordInput.getText().toString().length());

                    checkHit();

                    if (userEntered.length() == PIN_LENGTH) {

                        if (userEntered.equals(currentPin)) {

                            statusView.setTextColor(Color.GREEN);
                            statusView.setText("Correct PIN");
                            passwordInput.setText("");
                            userEntered="";

                            iteration++;
                            attemptCounter++;
                            pinIndex++;
                            int new_pin_index = pinIndex/10;
                            Log.v("AbstractUI", "Correct PIN");

                            if (attemptCounter == 10) {

                                getNextPin();
                            }

                            // Get next UI
                            if (new_pin_index==4) {

                                passwordField.setText("");
                                UISwitcher.getInstance(AbstractUI.this).getNextUI(AbstractUI.this);
                            }

                        } else {

                            iteration++;
                            statusView.setTextColor(Color.RED);
                            statusView.setText("Wrong PIN");
                            passwordInput.setText("");
                            userEntered="";
                            Log.v("AbstractUI", "Wrong PIN");
                        }
                    }

                } else {
                    // Roll over
                    passwordInput.setText("");
                    userEntered = "";
                    statusView.setText("");
                    userEntered = userEntered + pressedButton.getText();
                    passwordInput.setText(passwordInput.getText().toString() + "*");
                }
            }
        };

        button0 = (Button) findViewById(R.id.button0);
        button0.setOnClickListener(pinButtonHandler);

        button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(pinButtonHandler);

        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(pinButtonHandler);

        button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(pinButtonHandler);

        button4 = (Button) findViewById(R.id.button4);
        button4.setOnClickListener(pinButtonHandler);

        button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(pinButtonHandler);

        button6 = (Button) findViewById(R.id.button6);
        button6.setOnClickListener(pinButtonHandler);

        button7 = (Button) findViewById(R.id.button7);
        button7.setOnClickListener(pinButtonHandler);

        button8 = (Button) findViewById(R.id.button8);
        button8.setOnClickListener(pinButtonHandler);

        button9 = (Button) findViewById(R.id.button9);
        button9.setOnClickListener(pinButtonHandler);
    }

    // Listen to sensor events
    private SensorEventListener sensorListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor arg0, int arg1) {}

        @Override
        public void onSensorChanged(SensorEvent event) {
            Sensor sensor = event.sensor;
            if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

                aX = event.values[0];
                aY = event.values[1];
                aZ = event.values[2];

                a_values.add(aX);
                a_values.add(aY);
                a_values.add(aZ);

            } else if (sensor.getType() == Sensor.TYPE_GYROSCOPE) {

                gX = event.values[0];
                gY = event.values[1];
                gZ = event.values[2];

                g_values.add(gX);
                g_values.add(gY);
                g_values.add(gZ);

            } else if (sensor.getType() == Sensor.TYPE_ORIENTATION) {

                azimuth = event.values[0];
                pitch = event.values[1];
                roll = event.values[2];

                o_values.add(azimuth);
                o_values.add(pitch);
                o_values.add(roll);

            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sensor != null) {
            SM.unregisterListener(sensorListener);
        }
    }

    protected void onPause() {
        super.onPause();
        SM.unregisterListener(sensorListener);
    }

    protected void onResume() {
        super.onResume();
        SM.registerListener(sensorListener, SM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        SM.registerListener(sensorListener, SM.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_NORMAL);
        SM.registerListener(sensorListener, SM.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_NORMAL);
    }

    // Load current layout in subclass
    protected abstract int getLayoutResource();

    public void displayDialog() {

        if (UISwitcher.getInstance(AbstractUI.this).getPosture() == 0) {

            InfoDialogLeftHand infoLeft = new InfoDialogLeftHand(this);
            infoLeft.show(getFragmentManager(), "Left Hand Dialog");

        } else if (UISwitcher.getInstance(AbstractUI.this).getPosture() == 1) {

            InfoDialogRightHand infoRight = new InfoDialogRightHand(this);
            infoRight.show(getFragmentManager(), "Right Hand Dialog");
        }
    }

    private void setMyLayout() {

        myLayout = MyLayout.getInstance(AbstractUI.this);
    }

    // Checks if entered char was a hit or not
    public void checkHit() {

        if (userEntered.charAt(userEntered.length()-1) == currentPin.charAt(userEntered.length()-1)) {
            hit = 1;
            myLayout.onHit(currentPin, 1, initialTime, iteration);

        } else {
            hit = 0;
            myLayout.onHit(currentPin, 0, initialTime, iteration);
        }
    }

    public void getNextPin() {

        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(250);
        currentPin = pins.get((pinCounter+1) % pins.size());
        passwordField.setText("Enter Password: " + currentPin);
        Log.v("NEW PIN", "" + currentPin);
        attemptCounter = 0;
        iteration = 1;
        pinCounter++;
    }

    public int getTrial() {

        trial = UISwitcher.getInstance(AbstractUI.this).getIntTrial();
        return trial;
    }

    public static void returnToMainActivity() {

        Intent intent = new Intent(activity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }

    public void resetTrialFinished() {

        UISwitcher.getInstance(AbstractUI.this).resetTrialFinished();
    }

    public void setInitialTime() {

        this.initialTime = System.currentTimeMillis();
    }
}